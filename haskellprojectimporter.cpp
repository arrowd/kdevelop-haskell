/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "haskellsupport.h"
#include "haskellsettings.h"
#include "haskellprojectimporter.h"
#include "ui_haskellprojectimporter.h"

#include <interfaces/iproject.h>

#include <KMessageBox>
#include <KColorScheme>

#include <QDir>
#include <QPushButton>

#include <QJsonObject>

using namespace KDevelop;

#define STACK_ROW 2

HaskellProjectImporter::HaskellProjectImporter(IProject* project, ProjectImportData& projectData, QWidget* parent)
    : QDialog(parent), m_project(project), m_importData(projectData), m_stackRowHidden(false)
{
    setWindowTitle(i18n("Configure a Haskell project"));

    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    m_buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    auto mainWidget = new QWidget(this);
    auto mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    mainLayout->addWidget(mainWidget);

    m_Ui = new Ui::HaskellProjectImporter;
    m_Ui->setupUi(mainWidget);

    mainLayout->addWidget(m_buttonBox);

    OperationModes modes = HaskellUtils::supportedModes(project);
    if (!(modes & StackMode))
    {
        m_Ui->stackRadio->setEnabled(false);
    }
    if (!(modes & CabalMode))
    {
        m_Ui->cabalRadio->setEnabled(false);
    }

    if (HaskellUtils::createdFromStack(project)) {
        Q_ASSERT(modes & StackMode);
        m_mode = StackMode;
        m_Ui->stackRadio->setChecked(true);
        m_Ui->useCabalSandbox->setEnabled(false);
    }
    else {
        Q_ASSERT(modes & CabalMode);
        m_mode = CabalMode;
        m_Ui->cabalRadio->setChecked(true);
        m_Ui->useCabalSandbox->setEnabled(true);
    }

    m_Ui->stackBin->setText(HaskellSettings::stackExe());
    m_Ui->useStackForSearch->setChecked(HaskellSettings::useStackForSearch());
    m_Ui->cabalBin->setText(HaskellSettings::cabalExe());
    m_Ui->hieBin->setText(HaskellSettings::hieExe());

    connect(m_Ui->cabalRadio, &QRadioButton::toggled, [this](bool checked)
    {
        if (checked)
        {
            m_mode = CabalMode;
            if (!m_Ui->useStackForSearch->isChecked())
                hideStackRow();
            m_Ui->useCabalSandbox->setEnabled(true);
        }
        else
        {
            m_mode = StackMode;
            if (!m_Ui->useStackForSearch->isChecked())
                showStackRow();
            m_Ui->useCabalSandbox->setEnabled(false);
        }

        changed();
    });

    connect(m_Ui->useStackForSearch, &QCheckBox::toggled, [this](bool checked)
    {
        if (m_mode == CabalMode)
        {
            if (checked)
                showStackRow();
            else
                hideStackRow();
        }
    });

    connect(m_Ui->stackBin, &KUrlRequester::textChanged, this, &HaskellProjectImporter::changed);
    connect(m_Ui->cabalBin, &KUrlRequester::textChanged, this, &HaskellProjectImporter::changed);
    connect(m_Ui->hieBin,   &KUrlRequester::textChanged, this, &HaskellProjectImporter::changed);
    connect(m_Ui->buildDir, &KUrlRequester::textChanged, this, &HaskellProjectImporter::changed);

    connect(this, &QDialog::accepted, [this, project]()
    {
        //m_importData.flags = m_flagsModel.getJson();
        m_importData.mode = m_mode;
        m_importData.useStackForSearch = m_Ui->useStackForSearch->isChecked();
        m_importData.setupSandbox = m_Ui->useCabalSandbox->isChecked();

        m_importData.stack = stackOrDefault();
        m_importData.cabal = cabalOrDefault();
        m_importData.hie = hieOrDefault();

        m_importData.buildDir = m_Ui->buildDir->url().toString();
    });

    changed();
}

// stolen from CMakeBuildDirChooser
void HaskellProjectImporter::setStatus(const QString& message, bool canApply)
{
    KColorScheme scheme(QPalette::Normal);
    KColorScheme::ForegroundRole role;
    if (canApply) {
        role = KColorScheme::PositiveText;
    }
    else {
        role = KColorScheme::NegativeText;
    }
    m_Ui->status->setText(QStringLiteral("<i><font color='%1'>%2</font></i>").arg(scheme.foreground(role).color().name()).arg(message));

    auto okButton = m_buttonBox->button(QDialogButtonBox::Ok);
    okButton->setEnabled(canApply);
    if (canApply) {
        auto cancelButton = m_buttonBox->button(QDialogButtonBox::Cancel);
        cancelButton->clearFocus();
    }
}

void HaskellProjectImporter::changed()
{
    bool c = m_mode == StackMode || m_Ui->useStackForSearch->isChecked() ?
        checkExecutable(stackOrDefault()) :
        true;

    c   && checkExecutable(cabalOrDefault(), m_Ui->useStackForSearch->isChecked())
        && checkExecutable(hieOrDefault(), m_Ui->useStackForSearch->isChecked())
        && checkBuildDir();

    //m_Ui->flagsList->setModel(&m_flagsModel);
    //m_Ui->flagsList->setHeaderHidden(true);

    //auto cmd = new ListFlagsCommand(HaskellUtils::getModeString(m_mode), m_project->path().path());

    //auto hj = new HIEJob(m_project, m_importData.hieProcess, cmd);
    //connect(hj, &KJob::result, [hj, this] {
    //    m_flagsModel.setJson(hj->response.toArray());
        //m_Ui->flagsList->expandAll();
    //});
    //hj->start();
}

QString HaskellProjectImporter::stackOrDefault()
{
    QString ret = m_Ui->stackBin->text();

    if (ret.isEmpty())
        ret = HaskellSettings::stackExe();

    return ret;
}

QString HaskellProjectImporter::cabalOrDefault()
{
    QString ret = m_Ui->cabalBin->text();

    if (ret.isEmpty())
        ret = HaskellSettings::cabalExe();

    return ret;
}

QString HaskellProjectImporter::hieOrDefault()
{
    QString ret = m_Ui->hieBin->text();

    if (ret.isEmpty())
        ret = HaskellSettings::hieExe();

    return ret;
}

bool HaskellProjectImporter::checkExecutable(QString exe)
{
    if (HaskellUtils::findExecutable(exe).isEmpty()) {
        setStatus(i18n("Failed to locate %1 executable.", exe), false);
        return false;
    }
    
    return true;
}

bool HaskellProjectImporter::checkExecutable(QString exe, bool useStack)
{
    if (useStack) {
        // TODO: it would be nice to cache this
        if (HaskellUtils::findExecutable(exe, true, m_Ui->stackBin->text()).isEmpty()) {
            setStatus(i18n("Failed to locate %1 executable.", exe), false);
            return false;
        }
        return true;
    }
    
    return checkExecutable(exe);
}

bool HaskellProjectImporter::checkBuildDir()
{
    if (m_mode == CabalMode) {
        auto buildDirPath = m_Ui->buildDir->text();
        if (buildDirPath.isEmpty())
            buildDirPath = QStringLiteral("/dist");

        QFileInfo buildDirInfo(m_project->path().toLocalFile() + buildDirPath);

        if (!buildDirInfo.exists()) {
            setStatus(i18n("Creating a new build directory."), true);
            return true;
        }

        QDir buildDir(buildDirPath);

        if (buildDir.exists(QStringLiteral("setup-config"))) {
            setStatus(i18n("Using an already created build directory."), true);
            return true;
        }
        else {
            Q_ASSERT(buildDir.count() > 2);
            setStatus(i18n("The selected build directory is not empty."), false);
            return false;
        }
    }

    setStatus(i18n("Creating a new build directory."), true);
    return true;
}

void HaskellProjectImporter::hideStackRow()
{
    Q_ASSERT(!m_stackRowHidden);
    m_stackRowHidden = true;

    m_stackRow = m_Ui->formLayout->takeRow(STACK_ROW);
    m_stackRow.labelItem->widget()->setVisible(false);
    m_stackRow.fieldItem->widget()->setVisible(false);
}

void HaskellProjectImporter::showStackRow()
{
    Q_ASSERT(m_stackRowHidden);
    m_stackRowHidden = false;

    m_stackRow.labelItem->widget()->setVisible(true);
    m_stackRow.fieldItem->widget()->setVisible(true);
    m_Ui->formLayout->insertRow(STACK_ROW, m_stackRow.labelItem->widget(), m_stackRow.fieldItem->widget());
}

HaskellProjectImporter::~HaskellProjectImporter()
{
    delete m_Ui;
}

namespace Ui
{
    QModelIndex Model::index(int row, int column, const QModelIndex & parent) const
    {
        if (!parent.isValid())
            return createIndex(row, column, -1);

        return createIndex(row, column, parent.row());
    }

    QModelIndex Model::parent(const QModelIndex & index) const
    {
        if (index.internalId() == -1)
            return QModelIndex();
        return this->index(index.internalId(), index.column());
    }

    QVariant Model::data(const QModelIndex & index, int role) const
    {
        auto parent = index.parent();

        if (index.row() >= rowCount(parent))
            return QVariant();

        if (!parent.isValid())
            return role == Qt::DisplayRole
            ? d[index.row()].toObject()["name"].toString()
            : QVariant();

        if (role == Qt::DisplayRole)
            return d[parent.row()].toObject()["flags"].toArray()[index.row()].toObject()["name"];

        if (role == Qt::CheckStateRole)
        {
            bool v = d[parent.row()].toObject()["flags"].toArray()[index.row()].toObject()["value"].toBool();
            return v ? Qt::Checked : Qt::Unchecked;
        }

        if (role == Qt::BackgroundRole)
        {
            auto def = d[parent.row()].toObject()["flags"].toArray()[index.row()].toObject()["default"].toBool();
            auto val = d[parent.row()].toObject()["flags"].toArray()[index.row()].toObject()["value"].toBool();
            return def != val ? QBrush(QColor(239, 242, 132)) : QVariant();
        }

        return QVariant();
    }

    Qt::ItemFlags Model::flags(const QModelIndex & index) const
    {
        if (!index.parent().isValid())
            return Qt::ItemIsEnabled;
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    }

    int Model::rowCount(const QModelIndex & parent) const
    {
        if (!parent.isValid())
            return d.count();
        if (!parent.parent().isValid())
            return d[parent.row()].toObject()["flags"].toArray().count();
        return 0;
    }

    int Model::columnCount(const QModelIndex & parent) const
    {
        return 1;
    }

    bool Model::setData(const QModelIndex & index, const QVariant & value, int role)
    {
        auto parent = index.parent();

        if (!parent.isValid() || role != Qt::CheckStateRole)
            return false;

        setFlag(parent.row(), index.row(), value.toBool());
        dataChanged(index, index, { Qt::CheckStateRole });
        return true;
    }

    void Model::setFlag(int oid, int fid, bool v)
    {
        auto newflag = d[oid].toObject()["flags"].toArray()[fid].toObject();
        newflag["value"] = v;

        auto newflags = d[oid].toObject()["flags"].toArray();
        newflags[fid] = newflag;

        auto newobj = d[oid].toObject();
        newobj["flags"] = newflags;

        d[oid] = newobj;
    }
}

#include "haskellprojectimporter.moc"
