/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HASKELLUTILS_H
#define HASKELLUTILS_H

#include <QString>
#include <QFlags>
#include <KConfigGroup>

class ProjectData;
namespace KDevelop
{
    class IProject;
}

enum OperationMode
{
    CabalMode=1, StackMode=2
};
Q_DECLARE_FLAGS(OperationModes, OperationMode)
Q_DECLARE_OPERATORS_FOR_FLAGS(OperationModes)

namespace HaskellUtils
{
    QString findExecutable(QString exe, bool useStack = false, QString stackExe = QString());
    bool createdFromStack(KDevelop::IProject*);

    KConfigGroup getConfigGroup(KDevelop::IProject* p);
    bool isConfigured(ProjectData* pd);
    void setOperationMode(KDevelop::IProject*, OperationMode);
    OperationMode getOperationMode(KDevelop::IProject*);
    OperationModes supportedModes(KDevelop::IProject* p);
    QString getModeString(KDevelop::IProject*);
    QString getModeString(OperationMode mode);
}

#endif // HASKELLUTILS_H
