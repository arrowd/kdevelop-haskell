/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "haskellsupport.h"
#include "haskellsettings.h"
#include "haskellsettingspage.h"
#include "haskellprojectimporter.h"
#include "haskellprojectmodel.h"
#include "haskellimportjob.h"
#include "hiejobs/simplejobs.h"
#include "projectsettings.h"
#include "version.h"
#include "hiejobs/hiejob.h"
#include "haskellutils.h"

#include <interfaces/icore.h>
#include <interfaces/iproject.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iplugincontroller.h>
#include <project/interfaces/iprojectbuilder.h>
#include <interfaces/contextmenuextension.h>
#include <interfaces/idocumentcontroller.h>
#include <language/interfaces/iastcontainer.h>
#include <project/projectmodel.h>
#include <util/executecompositejob.h>

#include <language/assistant/staticassistantsmanager.h>
#include <language/assistant/renameassistant.h>
#include <language/backgroundparser/backgroundparser.h>
#include <language/codecompletion/codecompletion.h>
#include <language/codegen/basicrefactoring.h>
#include <language/highlighting/codehighlighting.h>
#include <language/interfaces/editorcontext.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/parsingenvironment.h>
#include <language/duchain/use.h>
#include <language/editor/documentcursor.h>

#include <KActionCollection>
#include <KPluginFactory>

#include <KTextEditor/View>
#include <KTextEditor/ConfigInterface>

#include <QAction>
#include <qreadwritelock.h>

K_PLUGIN_FACTORY_WITH_JSON(KDevHaskellSupportFactory, "kdevhaskellsupport.json", registerPlugin<HaskellSupport>(); )

using namespace KDevelop;

HaskellSupport::HaskellSupport(QObject* parent, const QVariantList& )
    : KDevelop::AbstractFileManagerPlugin( QStringLiteral("kdevhaskellsupport"), parent )
    , ILanguageSupport()
    , m_highlighting(nullptr)
    , m_refactoring(nullptr)
{
    setXMLFile( QStringLiteral("kdevhaskellsupport.rc") );
    // try to warm up stack, as it takes pretty long time to start for the first time
    QProcess::startDetached(HaskellSettings::stackExe(), { QStringLiteral("--help") });
}

HaskellSupport::~HaskellSupport()
{
    parseLock()->lockForWrite();
    // By locking the parse-mutexes, we make sure that parse jobs get a chance to finish in a good state
    parseLock()->unlock();
}

KJob* HaskellSupport::build(KDevelop::ProjectBaseItem *item)
{
    auto project = item->project();
    auto hieProc = m_projects[project]->hieProcess;
    auto mode = m_projects[project]->settings->operationMode();

    if (item->folder()) {
        return new BuildHJ(project, hieProc, mode, item->path());
    }

    if (item->target()->type() == ProjectBaseItem::ExecutableTarget) {
        return new BuildHJ(project, hieProc, mode,
            static_cast<CabalExecutableTargetItem*>(item->target())->stackComponentData());
    }

    if (item->target()->type() == ProjectBaseItem::LibraryTarget) {
        return new BuildHJ(project, hieProc, mode,
            static_cast<CabalLibraryTargetItem*>(item->target())->stackComponentData());
    }
}

KDevelop::ProjectFolderItem* HaskellSupport::import(IProject *project)
{
    ProjectData* pd = new ProjectData;
    ProjectSettings* ps = new ProjectSettings(project->projectConfiguration()->name());

    pd->project = project;

    if (! project->projectConfiguration()->group("ProjectSettings").hasKey("operationMode"))
    {
        ProjectImportData id;
        HaskellProjectImporter hpi(project, id);

        hpi.exec();

        if (hpi.result() == QDialog::Rejected)
            return nullptr;

        ps->setOperationMode(id.mode);
        ps->setStackExe(id.stack);
        ps->setCabalExe(id.cabal);
        ps->setHieExe(id.hie);
        ps->setUseStackForSearch(id.useStackForSearch);
        ps->setBuildDir(id.buildDir);
        ps->save();

        if (id.setupSandbox) {
            // TODO:
        }
    }
    else
        ps->load();


    QString hieExe = HaskellUtils::findExecutable(ps->hieExe(),
                                                  ps->useStackForSearch(),
                                                  ps->stackExe());

    QString stackPathEnv;

    if (ps->useStackForSearch()) {
        QProcess stackEnv;
        stackEnv.start(ps->stackExe(), { QStringLiteral("path"), QStringLiteral("--bin-path") });
        stackEnv.waitForFinished(-1);
        //TODO: is fromLocal8Bit ok?
        stackPathEnv = QString::fromLocal8Bit(stackEnv.readAllStandardOutput()).trimmed();
    }

    pd->hieProcess = new QProcess(project);
    pd->hieProcess->setProgram(hieExe);
    pd->hieProcess->setArguments({ QStringLiteral("-r"), project->path().toLocalFile() });
    pd->hieProcess->setWorkingDirectory(project->path().toLocalFile());
    if (!stackPathEnv.isEmpty()) {
        auto e = QProcessEnvironment::systemEnvironment();
        auto pathEnv = e.value(QStringLiteral("PATH"));
        e.insert(QStringLiteral("PATH"), pathEnv + ";" + stackPathEnv);
        pd->hieProcess->setProcessEnvironment(e);
    }
    pd->hieProcess->start();
    pd->hieProcess->waitForStarted(-1);
    if (pd->hieProcess->state() != QProcess::Running)
    {
        QMessageBox msgBox;
        msgBox.setText(i18n("Haskell Ide Engine process has failed to start"));
        msgBox.exec();
        return nullptr;
    }

    pd->settings = ps;
    m_projects[project] = pd;

    return AbstractFileManagerPlugin::import(project);
}

KJob* HaskellSupport::createImportJob(ProjectFolderItem* item)
{
    auto afmJob = AbstractFileManagerPlugin::createImportJob(item);
    auto hiJob = new HaskellImportJob(item, m_projects[item->project()], afmJob);

    connect(hiJob, &HIEJob::result, [this, item, hiJob]() {
        // after we filled buildFolders list, we delete existing
        // ProjectFolderItem's that correspond to them, and recreate
        // them as ProjectBuildFolderItem's
        auto& buildFolders = m_projects[item->project()]->buildFolders;

        for (auto i : item->children()) {
            auto folder = i->folder();
            if (!folder)
                continue;

            bool isBuildFolder = std::find(buildFolders.begin(), buildFolders.end(), folder->path()) != buildFolders.end();
            if (isBuildFolder)
                delete folder;
        }

        AbstractFileManagerPlugin::reload(item);

        hiJob->populateTargets(item);
    });

    return hiJob;
}

KDevelop::IProjectBuilder * HaskellSupport::builder() const
{
    KDevelop::IProjectBuilder* _builder = const_cast<HaskellSupport*>(this)->extension<KDevelop::IProjectBuilder>();
    Q_ASSERT(_builder);
    return _builder;
}

bool HaskellSupport::isValid(const Path& path, const bool isFolder, IProject* project) const
{
    if (isFolder && path.lastPathSegment() == QStringLiteral(".stack-work"))
        return false;

    return AbstractFileManagerPlugin::isValid(path, isFolder, project);
}

ProjectFolderItem* HaskellSupport::createFolderItem(IProject* project, const Path& path, ProjectBaseItem* parent)
{
    // project root is always a build folder. It is guaranteed that it contans stack.yaml or *.cabal
    if (project->path() == path) {
        return new KDevelop::ProjectBuildFolderItem(project, path, parent);
    }

    auto& buildFolders = m_projects[project]->buildFolders;

    auto folder = std::find(buildFolders.begin(), buildFolders.end(), path);

    if (folder != buildFolders.end())
        return new KDevelop::ProjectBuildFolderItem(project, path, parent);
    else
        return AbstractFileManagerPlugin::createFolderItem(project, path, parent);
}

QList<KDevelop::ProjectTargetItem*> HaskellSupport::targets() const
{
    QList<KDevelop::ProjectTargetItem*> ret;
    return ret;
}

KDevelop::Path HaskellSupport::buildDirectory(KDevelop::ProjectBaseItem*) const
{
    return KDevelop::Path();
}

QList<KDevelop::ProjectTargetItem*> HaskellSupport::targets(KDevelop::ProjectFolderItem* folder) const
{
    return QList<KDevelop::ProjectTargetItem*>();
}

KDevelop::ConfigPage* HaskellSupport::configPage(int number, QWidget* parent)
{
    if (number == 0) {
        return new HaskellSettingsPage(this, parent);
    }
    return nullptr;
}

int HaskellSupport::configPages() const
{
    return 1;
}

ParseJob* HaskellSupport::createParseJob(const IndexedString& url)
{
    return nullptr;
}

QString HaskellSupport::name() const
{
    return QStringLiteral("haskell");
}

ICodeHighlighting* HaskellSupport::codeHighlighting() const
{
    return m_highlighting;
}

BasicRefactoring* HaskellSupport::refactoring() const
{
    return m_refactoring;
}

void HaskellSupport::createActionsForMainWindow (Sublime::MainWindow* /*window*/, QString& _xmlFile, KActionCollection& actions)
{
    _xmlFile = xmlFile();

    //QAction* renameDeclarationAction = actions.addAction(QStringLiteral("code_rename_declaration"));
    //renameDeclarationAction->setText( i18n("Rename Declaration") );
    //renameDeclarationAction->setIcon(QIcon::fromTheme(QStringLiteral("edit-rename")));
    //actions.setDefaultShortcut(renameDeclarationAction, Qt::CTRL | Qt::SHIFT | Qt::Key_R);
    //connect(renameDeclarationAction, &QAction::triggered,
    //        m_refactoring, &BasicRefactoring::executeRenameAction);
}

KDevelop::ContextMenuExtension HaskellSupport::contextMenuExtension(KDevelop::Context* context)
{
    ContextMenuExtension cm;
    EditorContext *ec = dynamic_cast<KDevelop::EditorContext *>(context);

    if (ec && ICore::self()->languageController()->languagesForUrl(ec->url()).contains(this)) {
        // It's a C++ file, let's add our context menu.
        m_refactoring->fillContextMenu(cm, context);
    }
    return cm;
}

#include "haskellsupport.moc"
