/*
    This file is part of KDevelop Haskell Support

    Copyright 2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "simplejobs.h"
#include "haskellsupport.h"
#include "haskellsettings.h"

#include <interfaces/iproject.h>

#include <project/projectmodel.h>

#include <KShell>
#include <QProcess>
#include <QFileInfo>
#include <qdebug.h>

#include <functional>

using namespace KDevelop;

//
// ======= IsConfigured job ========
//

IsConfiguredHJ::IsConfiguredHJ(IProject* project, QProcess* proc,
                               OperationMode mode, const QString & distDir)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    if (!distDir.isEmpty())
        params["distDir"] = HIECommand::file(distDir);
    
    m_cmd.write("build:isConfigured", params);
}

void IsConfiguredHJ::handleResponse()
{
    if (response.isBool()) {
        m_isConfigured = response.toBool();
    }
    else {
        HIEJob::setErrorText("IsConfigured: response is not a bool");
    }
    emitResult();
}

bool IsConfiguredHJ::isConfigured()
{
    return m_isConfigured;
}

//
// ======= Configure job ========
//

ConfigureHJ::ConfigureHJ(IProject* project, QProcess* proc,
    OperationMode mode, const QString & distDir, const QString & buildTool)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    if (!distDir.isEmpty())
        params["distDir"] = HIECommand::file(distDir);
    if (!buildTool.isEmpty()) {
        QString key = mode == StackMode ? QStringLiteral("stackExe") : QStringLiteral("cabalExe");
        params[key] = HIECommand::text(buildTool);
    }

    m_cmd.write("build:configure", params);
}

void ConfigureHJ::handleResponse()
{
    emitResult();
}

//
// ======= Build job ========
//

BuildHJ::BuildHJ(IProject* project, QProcess* proc, OperationMode mode,
    Path directory)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    params["directory"] = HIECommand::file(directory.toLocalFile());

    m_cmd.write("build:buildDirectory", params);
}

BuildHJ::BuildHJ(IProject* project, QProcess* proc, OperationMode mode,
    QStringList componentData)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    params["package"] = HIECommand::text(componentData[0]);
    params["type"] = HIECommand::text(componentData[1]);
    if(componentData[1] != QStringLiteral("lib"))
        params["target"] = HIECommand::text(componentData[2]);

    m_cmd.write("build:buildTarget", params);
}

void BuildHJ::handleResponse()
{
    emitResult();
}

//
// ======= IsPrepared job ========
//

IsPreparedHJ::IsPreparedHJ(IProject* project, QProcess* proc,
    OperationMode mode, const QString & distDir)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    if (!distDir.isEmpty())
        params["distDir"] = HIECommand::file(distDir);

    m_cmd.write("build:isPrepared", params);
}

void IsPreparedHJ::handleResponse()
{
    if (response.isBool()) {
        m_isPrepared = response.toBool();
    }
    else {
        HIEJob::setErrorText("IsPreparedHJ: response is not a bool");
    }
    emitResult();
}

bool IsPreparedHJ::isPrepared()
{
    return m_isPrepared;
}

//
// ======= Prepare job ========
//

PrepareHJ::PrepareHJ(IProject* project, QProcess* proc,
    OperationMode mode, const QString & distDir, const QString & cabalExe)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    if (!distDir.isEmpty())
        params["distDir"] = HIECommand::file(distDir);
    if (!cabalExe.isEmpty()) {
        params["cabalExe"] = HIECommand::text(cabalExe);
    }

    m_cmd.write("build:prepare", params);
}

void PrepareHJ::handleResponse()
{
    emitResult();
}

#include "moc_simplejobs.cpp"
