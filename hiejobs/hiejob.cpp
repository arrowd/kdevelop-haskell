/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "hiejob.h"
#include "haskellsupport.h"
#include "haskellsettings.h"

#include <interfaces/iproject.h>

#include <project/projectmodel.h>

#include <KShell>
#include <QProcess>
#include <QFileInfo>
#include <qdebug.h>

#include <functional>

using namespace KDevelop;

HIEJob::HIEJob(IProject* project, QProcess* proc)
    : KJob(project->managerPlugin()), m_hieProc(proc), m_project(project)
{
    //setAutoDelete(false);
}

HIEJob::~HIEJob()
{
}

void HIEJob::start()
{
    if (m_hieProc->state() != QProcess::Running)
    {
        qDebug() << "HIEJob: haskell-ide-engine process isn't running: " << m_hieProc->errorString();
        setError(KJob::UserDefinedError);
        emitResult();
        return;
    }

    connect(m_hieProc, &QProcess::readyReadStandardOutput, this, &HIEJob::readyRead);
    connect(m_hieProc, &QProcess::readyReadStandardError, this, [this]() {
        qDebug() << "HIE error: " << m_hieProc->readAllStandardError();
    });

    m_hieProc->write(m_cmd.get());
}

void HIEJob::readyRead()
{
    auto bytes = m_hieProc->readAllStandardOutput();
    if (bytes.length() == 1 && bytes[0] == '\2')
        return;

    if (bytes.endsWith('\2'))
        bytes.chop(1);

    qDebug() << "HIE response: " << bytes;

    auto json = QJsonDocument::fromJson(bytes);

    if (!json.isObject())
    {
        qDebug() << "HIE returned JSON which isn't an object!";
        setError(KJob::UserDefinedError);
        return;
    }

    if (json.object().contains("Left")) {
        response = json.object()["Left"];
        setErrorText("HIE error");
        emitResult();
        return;
    }

    response = json.object()["Right"];

    handleResponse();
}

void HIEJob::setErrorText(const QString & error)
{
    QString errorMessage = response.toVariant().toString();
    qDebug() << error + errorMessage;
    setError(KJob::UserDefinedError);
    KJob::setErrorText(QStringLiteral("Invalid response: ") + errorMessage);
}

#include "moc_hiejob.cpp"
