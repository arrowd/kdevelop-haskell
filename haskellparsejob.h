#ifndef HASKELLPARSEJOB_H
#define HASKELLPARSEJOB_H

#include <serialization/indexedstring.h>
#include <language/backgroundparser/parsejob.h>
#include <language/duchain/duchainpointer.h>
#include <language/duchain/topducontext.h>
#include <language/duchain/problem.h>

class HaskellSupport;

class HaskellParseJob : public KDevelop::ParseJob
{
    Q_OBJECT
public:
    enum {
        Rescheduled = (KDevelop::TopDUContext::LastFeature << 1),
    };

    HaskellParseJob(const KDevelop::IndexedString &url, KDevelop::ILanguageSupport *languageSupport);

protected:
    void run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread *thread) override;

private:
    HaskellSupport *haskell() const;
};

#endif // HASKELLPARSEJOB_H
