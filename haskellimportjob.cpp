/*
    This file is part of KDevelop Haskell Support

    Copyright 2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "haskellimportjob.h"
#include "haskellprojectmodel.h"
#include "haskellsupport.h"
#include "projectsettings.h"
#include "hiejobs/simplejobs.h"
#include "haskellutils.h"

#include <interfaces/iproject.h>

#include <project/projectmodel.h>
#include <project/abstractfilemanagerplugin.h>
#include <project/interfaces/iprojectfilemanager.h>

#include <util/executecompositejob.h>

#include <functional>

#define WITH_ISPREPARED_JOB 0

using namespace KDevelop;

HaskellImportJob::HaskellImportJob(ProjectFolderItem* item, ProjectData* pd, KJob* afmJob)
    : KJob(item->project()->managerPlugin()), m_item(item), m_projectData(pd), m_AFBJob(afmJob),
    m_AFMJobDone(false), m_allHIEJobsDone(false)
{
}

void HaskellImportJob::start()
{
    auto p = m_item->project();
    auto isConfiguredJ = new IsConfiguredHJ(p, m_projectData->hieProcess,
        m_projectData->settings->operationMode(),
        m_projectData->settings->buildDir());

    connect(isConfiguredJ, &KJob::result, [this, isConfiguredJ]() {
        if (isConfiguredJ->error()) {
            emitError(isConfiguredJ->errorText());
            //delete isConfiguredJ;
            return;
        }
        m_isConfiguredResult = isConfiguredJ->isConfigured();
        //delete isConfiguredJ;
        jobDone(IsConfiguredJob);
    });

    isConfiguredJ->start();

    connect(m_AFBJob, &KJob::result, [this]() { jobDone(FileManagerJob); });
    m_AFBJob->start();
}

void HaskellImportJob::launchIsPreparedJob()
{
    auto isPreparedJ = new IsPreparedHJ(m_item->project(), m_projectData->hieProcess,
        m_projectData->settings->operationMode(),
        m_projectData->settings->buildDir());
    connect(isPreparedJ, &KJob::result, [this, isPreparedJ]() {
        if (isPreparedJ->error()) {
            emitError(isPreparedJ->errorText());
            //delete isPreparedJ;
            return;
        }
        m_isPreparedResult = isPreparedJ->isPrepared();
        //delete isPreparedJ;
        jobDone(IsPreparedJob);
    });

    isPreparedJ->start();
}

void HaskellImportJob::populateTargets(ProjectFolderItem* folder) {
    // find package for the current folder we are processing
    auto package = std::find_if(m_packages.begin(), m_packages.end(),
        [folder](const ListTargetsHJ::Package & p) {return KDevelop::Path(p.directory) == folder->path(); });

    if (package == m_packages.end())
        return;

    Q_ASSERT(!package->targets.empty());
    m_projectData->buildFolders.reserve(m_packages.size());
    m_projectData->buildFolders.push_back(folder->path());

    foreach(auto t, package->targets)
    {
        if (t.type == ListTargetsHJ::Target::Executable)
            new CabalExecutableTargetItem(folder->project(), t.name, package->name, folder);
        else if (t.type == ListTargetsHJ::Target::Library)
            new CabalLibraryTargetItem(folder->project(), t.name, package->name, folder);
    }

    foreach(auto f, folder->folderList())
        populateTargets(f);
}

void HaskellImportJob::launchListTargetsJob()
{
    auto listTargetsJ = new ListTargetsHJ(m_item->project(), m_projectData->hieProcess,
        m_projectData->settings->operationMode(),
        m_projectData->settings->buildDir());

    connect(listTargetsJ, &KJob::result, [this, listTargetsJ]()
    {
        m_packages = listTargetsJ->packages();
        //delete listTargetsJ;

        Q_ASSERT(!m_packages.empty());

        jobDone(ListTargetsJob);
    });

    listTargetsJ->start();
}

void HaskellImportJob::launchPrepareJob() {
    auto prepareJ = new PrepareHJ(m_item->project(), m_projectData->hieProcess,
        m_projectData->settings->operationMode(),
        m_projectData->settings->buildDir(),
        m_projectData->settings->cabalExe());

    connect(prepareJ, &KJob::result, [this, prepareJ]() {
        if (prepareJ->error()) {
            emitError(prepareJ->errorText());
            //delete prepareJ;
            return;
        }
        //delete prepareJ;
        jobDone(PrepareJob);
    });

    prepareJ->start();
}

void HaskellImportJob::jobDone(JobId j)
{
    switch (j) {
    case FileManagerJob:
        m_AFMJobDone = true;
        break;

    case IsConfiguredJob:
        if (!m_isConfiguredResult) {
            auto buildTool = m_projectData->settings->operationMode() == StackMode ?
                m_projectData->settings->stackExe() :
                m_projectData->settings->cabalExe();
            auto configureJ = new ConfigureHJ(m_item->project(), m_projectData->hieProcess,
                m_projectData->settings->operationMode(),
                m_projectData->settings->buildDir(),
                buildTool);

            connect(configureJ, &KJob::result, [this, configureJ]() {
                if (configureJ->error()) {
                    emitError(configureJ->errorText());
                    //delete configureJ;
                    return;
                }
                //delete configureJ;
                jobDone(ConfigureJob);
            });

            configureJ->start();
        }
        else
#if WITH_ISPREPARED_JOB
            launchIsPreparedJob();
#else
            launchPrepareJob();
#endif
        break;

    case ConfigureJob:
#if WITH_ISPREPARED_JOB
        launchIsPreparedJob();
#else
        m_isPreparedResult = false;
        // here we recursively call jobDone. This is OK until this function is reentrant
        jobDone(IsPreparedJob);
#endif
        break;

    case IsPreparedJob:
        // TODO: warning dialog here
        if (!m_isPreparedResult)
            launchPrepareJob();
        else
            launchListTargetsJob();
        break;

    case PrepareJob:
        launchListTargetsJob();
        break;

    case ListTargetsJob:
        m_allHIEJobsDone = true;
        break;
    }

    if (m_AFMJobDone && m_allHIEJobsDone) // all done
        emitResult();
}

void HaskellImportJob::emitError(const QString & error)
{
    setError(KJob::UserDefinedError);
    setErrorText(error);
    emitResult();
}

#include "moc_haskellimportjob.cpp"
