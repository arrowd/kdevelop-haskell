/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HASKELLPROJECTIMPORTER_H
#define HASKELLPROJECTIMPORTER_H

#include "haskellutils.h"

#include <QDialog>
#include <QStandardItemModel>
#include <QJsonArray>
#include <QFormLayout>

#include <util/path.h>

class QDialogButtonBox;
struct ProjectImportData;

namespace KDevelop {
    class IProject;
}

namespace Ui {
    class HaskellProjectImporter;

    class Model : public QAbstractItemModel
    {
    public:
        QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
        QModelIndex parent(const QModelIndex & index) const;
        QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
        Qt::ItemFlags flags(const QModelIndex & index) const;
        bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
        int rowCount(const QModelIndex & parent = QModelIndex()) const;
        int columnCount(const QModelIndex & parent = QModelIndex()) const;

        void setJson(QJsonArray a)
        {
            beginResetModel();
            d = a;
            endResetModel();
        }
        QJsonArray getJson() const
        {
            return d;
        }

    private:
        void setFlag(int oid, int fid, bool v);
        QJsonArray d;
    };

}

struct ProjectImportData
{
    OperationMode mode;
    bool useStackForSearch, setupSandbox;
    QString stack, cabal, hie;
    QString buildDir;
};

class HaskellProjectImporter : public QDialog
{
    Q_OBJECT
public:
    HaskellProjectImporter(KDevelop::IProject* project, ProjectImportData& projectData, QWidget* parent = 0);
    ~HaskellProjectImporter() override;

private slots:
    void changed();

private:
    void setStatus(const QString& message, bool canApply);
    void hideStackRow();
    void showStackRow();
    QString stackOrDefault();
    QString cabalOrDefault();
    QString hieOrDefault();
    bool checkExecutable(QString exe);
    bool checkExecutable(QString exe, bool useStack);
    bool checkBuildDir();

    Ui::Model m_flagsModel;
    OperationMode m_mode;
    KDevelop::IProject* m_project;
    ProjectImportData& m_importData;
    bool m_stackRowHidden;
    QFormLayout::TakeRowResult m_stackRow;

    Ui::HaskellProjectImporter* m_Ui;
    QDialogButtonBox* m_buttonBox;
};


#endif
