/*
    This file is part of KDevelop Haskell Support

    Copyright 2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HASKELLIMPORTJOB_H
#define HASKELLIMPORTJOB_H

#include "hiejobs/listtargetsjob.h"

#include <util/path.h>

#include <KJob>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace KDevelop
{
class ProjectFolderItem;
class ReferencedTopDUContext;
}

class ProjectData;

class HaskellImportJob : public KJob
{
    Q_OBJECT

public:
    HaskellImportJob(KDevelop::ProjectFolderItem* item, ProjectData* pd, KJob* afmJob);

    void start() override;

    void populateTargets(KDevelop::ProjectFolderItem* item);

private:
    enum JobId {
        IsConfiguredJob,
        ConfigureJob,
        IsPreparedJob,
        PrepareJob,
        ListTargetsJob,
        FileManagerJob
    };

private Q_SLOTS:
    void jobDone(JobId);

private:
    void launchIsPreparedJob();
    void launchPrepareJob();
    void launchListTargetsJob();
    void emitError(const QString & error);

    bool m_AFMJobDone, m_allHIEJobsDone;
    bool m_isConfiguredResult, m_isPreparedResult;

    KJob* m_AFBJob;
    KDevelop::ProjectFolderItem* m_item;
    ProjectData* m_projectData;
    QVector<ListTargetsHJ::Package> m_packages;
};



#endif // HASKELLIMPORTJOB_H
